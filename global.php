<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>One of the strengths project</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
	<link type='text/css' rel='stylesheet' href='style.css' /> 
	
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
    <header>
		<nav class="navbar navbar-default menu">
		  <div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="index.php" style="color: #f76d50;">One of the strengths project</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			 <ul class="nav navbar-nav">
				<li ><a href="#">главная</a></li>
				<li ><a href="https://vk.com/ootsp">мы вконтакте</a></li>
			 </ul>

			  
			  
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container -->
		</nav>
	</header>
	
	<div class="content">
	
		<div class="splash">
			<div class="container">
				<div class="slogan">
					<h1><b>One of the strengths project предлагает войти в админ панель: </b></h1>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12"><div class="slogan">
					<h1><b>Админ панель</b></h1>
					<p>
                        Админ панель OOTSP предоставляет следующие возможности:<br>
                        - Полное управление аккаунтов (настройка: Логина (nick аккаунта), Email, прав администратора, аватар, пола, статуса аккаунта и пароля).<br>
                        - Включение/выключение плагинов из списка доступных (в разработке).<br>
                        - Настройка своего аккаунта.<br>
                        - Изменение языка админ панели (на данный момент RU/EN).<br>
                        Мы используем API (пока в тестовом режиме).<br><br>
                        Готов опробовать?<br>
                        Тогда жми "Подключится".
                    </p>
                            <br>
                    <div class="card" ><a href="/admin">подключиться</a></div></div>
				</div>



				</div>
			</div>
		</div>
		
	</div>

	<footer>
	<hr>
		<div class="container">
			<p class="pull-right" style="text-align: right;">© 2019 OOTSP</p>
		</div>
	</footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>