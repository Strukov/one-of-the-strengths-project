-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Фев 20 2019 г., 19:16
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13
-- вв

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `adminpanel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `admin_access` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `archive` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id`, `user`, `admin_access`, `created_at`, `modified_at`, `modified_by`, `active`, `archive`, `deleted`) VALUES
(1, 1, 7, '0000-00-00 00:00:00', '2019-02-17 20:00:51', 1, 1, 0, 0),
(9, 3, 4, '2019-02-16 05:24:19', '2019-02-16 22:49:58', 1, 1, 0, 0),
(10, 23, 7, '2019-02-20 19:13:55', '0000-00-00 00:00:00', 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `sex` int(11) NOT NULL,
  `token` text NOT NULL,
  `reg_ip` text NOT NULL,
  `use_ip` text NOT NULL,
  `images` text NOT NULL,
  `created_at` datetime NOT NULL,
  `last_login_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `archive` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `email`, `sex`, `token`, `reg_ip`, `use_ip`, `images`, `created_at`, `last_login_at`, `modified_at`, `modified_by`, `active`, `archive`, `deleted`) VALUES
(1, 'Rikko', '123', 'strukov.pavel.n@gmail.com', 1, '', '127.0.0.1', '127.0.0.1', 'MjSbCTu3RgM.jpg', '2019-01-15 04:28:13', '2019-02-20 18:03:41', '2019-02-17 20:00:51', 1, 1, 0, 0),
(2, 'Victor', '123', 'email1@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '2019-02-16 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(3, 'Roman', '123', 'email2@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '1022.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-02-16 22:49:58', 1, 0, 1, 0),
(4, 'Pyzo1', '123', 'email3@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 1),
(5, 'Pyzo2', '123', 'email4@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(6, 'Pyzo3', '123', 'email5@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(7, 'Pyzo4', '123', 'email6@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', 'programmer.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-02-17 20:00:28', 1, 1, 0, 0),
(8, 'Pyzo5', '123', 'email7@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(9, 'Pyzo6', '123', 'email8@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(10, 'Pyzo7', '123', 'email9@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(11, 'Pyzo8', '123', 'email10@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(12, 'Pyzo9', '123', 'email11@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(13, 'Chebyrek1', '123', 'email12@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(14, 'Chebyrek2', '123', 'email13@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-02-16 22:26:30', 1, 1, 0, 0),
(15, 'Chebyrek3', '123', 'email14@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(16, 'Chebyrek4', '123', 'email15@gmail.com', 1, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(17, 'Chebyrek5', '123', 'email16@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(18, 'Chebyrek6', '123', 'email17@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(19, 'Chebyrek7', '123', 'email18@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(20, 'Chebyrek8', '123', 'email19@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(21, 'Chebyrek9', '123', 'email20@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(23, 'admin', 'admin', 'admin@gmail.com', 0, 'tyddlaypaw6c56vxq286yi93rya0uw9o6bn', '127.0.0.1', '', '', '2019-02-20 19:13:37', '0000-00-00 00:00:00', '2019-02-20 19:13:55', 1, 1, 0, 0),
(24, 'admins', 'admin', 'admins@gmail.com', 0, 'weyijpf23h4mzn2bx3fvipdfi3qhnmpmtbg', '127.0.0.1', '', '', '2019-02-20 19:14:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `use_plugins`
--

CREATE TABLE IF NOT EXISTS `use_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `lang-name` text NOT NULL,
  `lang-info` text NOT NULL,
  `active` tinyint(1) NOT NULL,
  `archive` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `use_plugins`
--

INSERT INTO `use_plugins` (`id`, `name`, `lang-name`, `lang-info`, `active`, `archive`, `deleted`) VALUES
(1, 'Новостная лента', 'BUTTON_3', 'INFO_PLUGINS_1', 1, 0, 0),
(2, 'Добавить аккаунт', 'BUTTON_400', 'INFO_PLUGINS_400', 1, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
