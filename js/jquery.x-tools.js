/*
 * jQuery x-Tools v1.5.0
 * https://github.com/x-three/x-tools
 *
 * Copyright 2013, Vladimir Volkov.
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */



/* Helpers ****************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.xHelpers = {};



    $.xHelpers.selector2object = function(oThis, $ancestor) {
        $.each(oThis, function(key, value) {
            if (/^s[A-Z]/.test(key)) {
                key = '$' + key.charAt(1).toLowerCase() + key.substr(2);
                oThis[key] = $.type(value) === 'string' ? $ancestor.find(value) : value;
            }
        });
    };



    $.xHelpers.XToggleContent = function() {
        $.extend(this, {
            isOpen:    false,
            duration:  300,
            sClose:    '.xs-close',
            bodyClose: false
        });
    };

    $.xHelpers.XToggleContent.prototype = {

        _bindHandlers: function() {
            this.$handle.on('click', $.proxy(this.show, this));
            if (this.isOpen) {
                this.bodyClose && this.$body.on('click', $.proxy(this.hide, this));
            }
        },

        show: function(ev) {
            if (!this.isOpenNext) {
                this.isOpenNext = true;
                this.$handle.off('click', this.show);
                setTimeout($.proxy(function() {
                    this.$handle.on('click', $.proxy(this.hide, this));
                    this.bodyClose && this.$body.on('click', $.proxy(this.hide, this));
                }, this), 50);
                this._update();
            }
            ev.preventDefault();
        },

        hide: function(ev) {
            if (this.$close.is(ev.target) || !this.$content.find(ev.target).length) {
                this.$body.off('click', this.hide);
                this.$handle.off('click', this.hide);
                this.isOpenNext = false;
                setTimeout($.proxy(function() {
                    this.$handle.on('click', $.proxy(this.show, this));
                }, this), 50);
                this._update();
            }
            ev.preventDefault();
        },

        _update: function() {
            if (this.busy || this.isOpen === this.isOpenNext) { return; }
            this.busy = true;

            this._change().done($.proxy(function() {
                this.isOpen = !this.isOpen;
                this.busy = false;
                this._update();
            }, this));
        },

        _change: function() {
            throw 'Not implemented yet!';
        }
    };
}(jQuery));
/* End Helpers ************************************************************************************************************************************************/



/* Init Events ************************************************************************************************************************************************/
(function($) {
    'use strict';

    $('html').removeClass('no-js');

    $(function() {
        $('html').addClass('dom-ready');
    });

    $(window).load(function() {
        $('html').addClass('win-load');
    });
}(jQuery));
/* End Init Events ********************************************************************************************************************************************/



/* Browser Identification *************************************************************************************************************************************/
(function($) {
    'use strict';

    var XBList = function() {
        this.aList = [];
    };

    XBList.prototype = {

        toCondition: function(c) {
            return c === undefined ? {} : ($.isPlainObject(c) ? c : { userAgent: c });
        },

        add: function(name, allow, deny) {
            this.aList.push({
                name: name,
                oAllow: this.toCondition(allow),
                oDeny: this.toCondition(deny)
            });
            return this;
        },

        get: function() {
            var i, j, state;
            for (i = 0; i< this.aList.length; i++) {
                state = true;
                for (j in this.aList[i].oAllow) {
                    if (!this.aList[i].oAllow[j].test(navigator[j])) {
                        state = false;
                        break;
                    }
                }
                for (j in this.aList[i].oDeny) {
                    if (this.aList[i].oDeny[j].test(navigator[j])) {
                        state = false;
                        break;
                    }
                }
                if (state) {
                    return this.aList[i].name;
                }
            }
            return null;
        }
    };

    var oName = new XBList(),
        oOS = new XBList(),
        oPlatform = new XBList(),
        oEngine = new XBList(),
        getVersion = function() {
            try {
                return (/.+(Chrome|Firefox|Version|OPR|MSIE|Trident.+?rv|CriOS)(\/| |:)([0-9\.]+)/).exec(navigator.userAgent)[3] || null;
            } catch (e) {
                return 0;
            }
        },
        fnUpdate = function() {
            $.xBrowser = {
                name: oName.get(),
                os: oOS.get(),
                platform: oPlatform.get(),
                engine: oEngine.get(),
                version: getVersion()
            };
            $('html')
                .removeClass('chrome firefox safari opera ie android ios win mac linux mobile desktop webkit gecko presto trident')
                .addClass($.xBrowser.os + ' ' + $.xBrowser.platform + ' ' + $.xBrowser.engine + ' ' + $.xBrowser.name + ' ' + $.xBrowser.name + parseInt($.xBrowser.version, 10));
        };

    oName .add('chrome', { userAgent: /Chrome/, vendor: /Google/ })
        .add('chrome', { userAgent: /CriOS/ })
        .add('firefox', /Firefox/)
        .add('safari', { userAgent: /Safari/, vendor: /Apple/ })
        .add('opera', /Opera/)
        .add('opera', { vendor: /Opera/})
        .add('ie', /MSIE|Trident/);

    oOS .add('android', /Android/) .add('ios', { platform: /iPad|iPhone|iPod/ }) .add('ios', /iPad|iPhone|iPod/) .add('win', /Windows/) .add('mac', /Macintosh/) .add('linux', /Linux/);
    oPlatform .add('mobile', { platform: /Android|iPad|iPhone|iPod/ }) .add('mobile', /Android|iPad|iPhone|iPod/) .add('desktop', /Windows|Macintosh|Linux/);
    oEngine .add('webkit', /WebKit/) .add('gecko', /Gecko/, /WebKit/) .add('presto', /Presto/) .add('trident', /Trident|MSIE (7|6|5)/);

    fnUpdate();
}(jQuery));
/* End Browser Identification *********************************************************************************************************************************/



/* IE lte 8 HTML5 support *************************************************************************************************************************************/
(function($) {
    'use strict';

    if ($.xBrowser.name === 'ie' && $.xBrowser.version < 9) {
        var tags = [
            'article',
            'aside',
            'figcaption',
            'figure',
            'footer',
            'header',
            'hgroup',
            'mark',
            'nav',
            'section',
            'time'
        ];
        for (var i = 0; i < tags.length; i++) {
            document.createElement(tags[i]);
        }
    }
}(jQuery));
/* End IE lte 8 HTML5 support *********************************************************************************************************************************/



/* Events *****************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.xEvent = {
        bind: function(name, weight, cb) {
            if ($.type(weight) !== 'number') {
                cb = arguments[1];
                weight = 0;
            }
            add(name, weight, cb);
            return this;
        },

        unbind: function(name, cb) {
            remove(name, cb);
            return this;
        },

        trigger: function(name) {
            var aArg = Array.prototype.slice.call(arguments, 1),
                aNamePart = name.split('.'),
                aNameStack = [aNamePart[0]],
                i;

            for (i = 1; i < aNamePart.length; i++) {
                aNameStack[i] = aNameStack[i - 1] + '.' + aNamePart[i];
            }
            aNameStack.reverse();

            for (i = 0; i < aNameStack.length; i++) {
                fire.apply(name, [aNameStack[i]].concat(aArg));
            }

            return this;
        }
    };

    var oList = {},

        fire = function(name) {
            if (oList[name]) {
                for (var i = 0; i < oList[name].length; i++) {
                    oList[name][i].cb.apply({ name: name, fullname: this }, Array.prototype.slice.call(arguments, 1));
                }
            }
        },

        add = function(name, weight, cb) {
            if (!oList[name]) {
                oList[name] = [];
            }
            if (index(name, cb) === -1) {
                var obj = { weight: weight, cb: cb };
                for (var i = 0; i < oList[name].length; i++) {
                    if (obj.weight > oList[name][i].weight) {
                        oList[name].splice(i, 0, obj);
                        return;
                    }
                }
                oList[name].push(obj);
            }
        },

        remove = function(name, cb) {
            var i;
            if (oList[name] && (i = index(name, cb)) !== -1) {
                oList[name].splice(i, 1);
            }
        },

        index = function(name, cb) {
            for (var i = 0; i < oList[name].length; i++) {
                if (oList[name][i].cb === cb) {
                    return i;
                }
            }
            return -1;
        };
}(jQuery));
/* End Events *************************************************************************************************************************************************/



/* Object *****************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.xObject = function(aKeys, aValues) {
        var obj = {};
        if ($.isArray(aKeys) && $.isArray(aValues)) {
            $.each(aKeys, function(i, key) {
                obj[key] = aValues[i];
            });
        } else if ($.isArray(aKeys)) {
            $.each(aKeys, function(i, key) {
                obj[key] = aValues;
            });
        } else {
            obj[aKeys] = aValues;
        }
        return obj;
    };
}(jQuery));
/* End Object *************************************************************************************************************************************************/



/* Arguments **************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.xArguments = function(aConditions, aParams, aDefault) {
        aDefault = aDefault || [];

        var aResult = [],
            i, j;

        for (i = 0, j = 0; i < aConditions.length; i++) {
            if (
                aConditions[i] === null ||
                aConditions[i] === 'easing' && !!$.easing[aParams[j]] ||
                $.type(aConditions[i]) === 'string' ? $.type(aParams[j]) === aConditions[i] : aConditions[i](aParams, j)
            ) {
                aResult[i] = aParams[j];
                j++;
            } else {
                aResult[i] = aDefault[i];
            }
        }

        return aResult;
    };
}(jQuery));
/* End Arguments **********************************************************************************************************************************************/



/* Hidden CSS *************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.fn.xHiddenCSS = function(prop) {
        var el = this[0];
        if (window.getComputedStyle) {
            return window.getComputedStyle(el)[prop];
        } else {
            return getIEComputedStyle(el, prop);
        }
    };

    var getIEComputedStyle = function(el, prop) {
        var value = el.currentStyle[prop] || 0;

        var leftCopy = el.style.left;
        var runtimeLeftCopy = el.runtimeStyle.left;

        el.runtimeStyle.left = el.currentStyle.left;
        el.style.left = (prop === "fontSize") ? "1em" : value;
        value = el.style.pixelLeft + "px";

        el.style.left = leftCopy;
        el.runtimeStyle.left = runtimeLeftCopy;

        return value;
    };
}(jQuery));
/* End Hidden CSS *********************************************************************************************************************************************/



/* CSS Animate ************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.fn.xCssAnimate = function(clazz, oProperties) {
        var fxClazz = 'fx';

        if ($.type(clazz) !== 'string') {
            oProperties = arguments[0];
            clazz = '';
        } else {
            fxClazz = 'fx-' + clazz;
        }

        return this.each(function() {
            var $el = $(this),
                dComplete = $.Deferred();

            $el.queue(function(fxDequeue) {
                dComplete.done(function() {
                    fxDequeue();
                });
            });

            setTimeout(function() {
                $el.addClass(fxClazz + ' ' + clazz).css(oProperties || {});
                setTimeout(function() {
                    $el.removeClass(fxClazz);
                    dComplete.resolve();
                }, getDelay($el));
            }, 10);
        });
    };

    var getDelay = function(el) {
            var $el = $(el),
                aDuration = getTimeArray($el, 'transition-duration'),
                aDelay = getTimeArray($el, 'transition-delay'),
                aResult = [];

            for (var i = 0; i < aDuration.length; i++) {
                aResult[i] = aDuration[i] + aDelay[i];
            }

            return Math.max.apply(null, aResult.concat(0));
        },

        getTimeArray = function(el, property) {
            return $.map($(el).css(property).split(', '), function(value) {
                return parseFloat(value) * 1000;
            });
        };
}(jQuery));
/* End CSS Animate ********************************************************************************************************************************************/



/* Frame Animate **********************************************************************************************************************************************/
(function($) {
    'use strict';

    $.xFrameAnimate = function(params) {
        return new FrameAnimate(params);
    };

    var FrameAnimate = function(params) {
        this._initialize(params);
    };

    $.extend(FrameAnimate.prototype, {

        frame:
            window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function(callback) {
                window.setTimeout(callback, 1000 / 30);
            },

        _initialize: function(params) {
            this.cfg = $.extend({
                duration: 1000,
                cbStart: $.noop,
                cbStep: $.noop,
                cbEnd: $.noop,
                context: this
            }, params);
        },

        fx: function() {
            if (!this.run) {
                return;
            }

            var now = Date.now();
            this.pos = Math.min(this.cfg.duration, now - this.tStart) / this.cfg.duration;

            if (this.pos < 1) {
                if (this.cfg.cbStep.call(this.cfg.context, this.pos) !== false) {
                    this.frame.call(window, $.proxy(function() {
                        this.fx();
                    }, this));
                }
            } else {
                this.cfg.cbStep.call(this.cfg.context, this.pos);
                this.stop();
            }
        },

        start: function() {
            $.extend(this, {
                tStart: Date.now(),
                pos: 0,
                run: true
            });
            this.cfg.cbStart.call(this.cfg.context, this.pos);
            this.fx();
        },

        stop: function() {
            this.run = false;
            this.cfg.cbEnd.call(this.cfg.context, this.pos);
            this.pos = 1;
        },

        pause: function() {
            this.run = false;
        },

        resume: function() {
            $.extend(this, {
                tStart: Date.now() - this.cfg.duration * this.pos,
                run: true
            });
            this.fx();
        }

    });
}(jQuery));
/* End Frame Animate ******************************************************************************************************************************************/



/* Math *******************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.xMath = {
        floor: function(num, order) {
            return fnRound(num, order, Math.floor);
        },

        round: function(num, order) {
            return fnRound(num, order, Math.round);
        },

        ceil: function(num, order) {
            return fnRound(num, order, Math.ceil);
        }
    };

    var fnRound = function(num, order, fn) {
            if (!order) {
                return fn(num);
            } else if ($.type(order) !== 'number') {
                return NaN;
            } else if (order % 1 === 0) {
                return fnRoundInt(num, order, fn);
            } else if (order > -1 && order < 1) {
                return fnRoundFract(num, order, fn);
            } else {
                return NaN;
            }
        },

        fn2StringObj = function(num) {
            var aNum = /^(-?[0-9]+)(?:\.([0-9]+))?$/.exec(num.toString());
            return {
                i: aNum[1] || '',
                f: aNum[2] || ''
            };
        },

        fnRoundInt = function(num, order, fn) {
            var oNum = fn2StringObj(num),
                l = oNum.i.length,
                r = Math.max(0, Math.min(l, (order > 0 ? order : l + order))),
                k = Number('1e' + r);
            return fn(num / k) * k;
        },

        fnRoundFract = function(num, order, fn) {
            var oNum = fn2StringObj(num),
                l = oNum.f.length,
                r = (function() {
                    var r = Number(fn2StringObj(order).f);
                    r = order > 0 ? (l - r) : r;
                    return Math.max(0, Math.min(l, (r)));
                }()),
                f = oNum.f.length === r ? 0 : fn(Number(oNum.f) / Number('1e' + r)) / Number('1e' + (l - r));
            return Number(oNum.i) + f;
        };
}(jQuery));
/* End Math ***************************************************************************************************************************************************/



/* Cookies ****************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.xCookie = {
        add: function(name, value, params) {
            set($.extend(
                $.xObject(name, value),
                params
            ));
            return this;
        },

        remove: function(name, params) {
            set($.extend(
                $.xObject([ name, 'expires' ], [ '', new Date() ]),
                params
            ));
            return this;
        },

        get: function(name) {
            return get()[name];
        },

        all: function() {
            return get();
        }
    };

    var get = function() {
            var result = {};
            $.each(document.cookie.split('; '), function() {
                var cookie = this.split('=');
                result[cookie[0]] = cookie[1];
            });
            return result;
        },

        set = function(params) {
            switch ($.type(params.expires)) {
                case 'number':
                    params.expires = (new Date((new Date()).getTime() + params.expires * 1000 * 60 * 60 * 24)).toUTCString();
                    break;
                case 'date':
                    params.expires = params.expires.toUTCString();
                    break;
            }
            document.cookie = $.map(params, function(el, index) {
                return index + '=' + el;
            }).join(';');
        };
}(jQuery));
/* End Cookies ************************************************************************************************************************************************/



/* Hash *******************************************************************************************************************************************************/
(function($) {
    'use strict';

    var wl = window.location;

    $.xHash = {
        get: function() {
            return decodeURIComponent(wl.hash.substr(1));
        },

        set: function(hash) {
            var h = '#' + encodeURIComponent(hash);
            wl.hash = h;
            return h;
        },

        replace: function(hash) {
            var h = '#' + encodeURIComponent(hash);
            wl.replace(wl.protocol + '//' + wl.hostname + wl.pathname + wl.search + h);
            return h;
        }
    };
}(jQuery));
/* End Hash ***************************************************************************************************************************************************/



/* Preload ****************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.fn.xPreload = function() {
        return fnPreload($.map(this.filter('img').toArray(), function() {
            return $(this).attr('src');
        }), this);
    };

    $.xPreload = function(aSrc) {
        return fnPreload($.isArray(aSrc) ? aSrc : [aSrc], aSrc);
    };

    var fnPreload = (function() {
        var items = 0,

            fnCreate = function() {
                return items++ === 0 ? $('<div id="x-preload" style="display: none;"></div>').appendTo('body') : $('#x-preload');
            },

            fnDestroy = function() {
                if (--items === 0) {
                    $('#x-preload').remove();
                }
            },

            fnLoad = function(src) {
                if (/^.+\.(jpg|jpeg|png|gif)$/.test(src)) {
                    var dComplete = $.Deferred();
                    $('<img>').on('load', function() {
                        $(this).detach();
                        fnDestroy();
                        dComplete.resolve();
                    }).attr('src', src).appendTo(fnCreate());
                    return dComplete;
                } else if (/^.+\.(js|css)$/.test(src)) {
                    return $.get(src);
                } else {
                    return null;
                }
            };


        return function(array, arg) {
            var dComplete = $.Deferred(),
                dArray = [];

            for (var i = 0; i < array.length; i++) {
                dArray.push(fnLoad(array[i]));
            }
            $.when.apply(null, dArray).done(function() {
                dComplete.resolve(arg);
            });

            return dComplete;
        };
    }());
}(jQuery));
/* End Preload ************************************************************************************************************************************************/



/* Style ******************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.fn.xStyle = function(params) {
        params = $.extend(true, {
            preload: [],
            clazz: {
                el: {
                    wrap:        'x-style-wrap',
                    title:       'e-title',
                    holder:      'e-holder',
                    placeholder: 'e-placeholder',
                    scroll:      'r-scroll',
                    handle:      'e-handle'
                },
                is: {
                    empty:       'is-empty',
                    focus:       'is-focus',
                    hover:       'is-hover',
                    active:      'is-active',
                    placeholder: 'is-placeholder',
                    disabled:    'is-disabled'
                },
                m: {
                    mobile:      'm-mobile',
                    hide:        'm-hide'
                }
            },
            text:       '<div class="x-text">#element#<i class="bg"></i></div>',
            password:   '<div class="x-password">#element#<i class="bg"></i></div>',
            textarea:   '<div class="x-textarea">#element#<i class="bg"></i>#scroll#</div>',
            checkbox:   '<a class="x-checkbox" href="javascript:void(0)">#element#<i class="bg"></i></a>',
            radio:      '<a class="x-radio" href="javascript:void(0)">#element#<i class="bg"></i></a>',
            button:     '<div class="x-button">#element#<span class="e-title"></span><i class="bg"></i></div>',
            select:     '<div class="x-select">#element#<span class="e-title"></span><i class="bg"></i><i class="e-icon"></i></div>',
            selectDrop: '<div class="x-select-drop"><ul class="r-list">#list#</ul><i class="bg"></i></div>',
            selectItem: '<li>#title#</li>',
            file:       '<div class="x-file"><div class="e-holder">#element#</div><span class="e-title"></span></div>',
            fileFormat: 'File: $name.$ext'
        }, params);

        var $el = $($.grep(this.toArray(), function(el) {
                return fnGetType($(el)) && $(el).data('x-style-complete') === undefined;
            })),
            $result = $([]);

        $result
            .add(fnStyleText($el, params))
            .add(fnStylePassword($el, params))
            .add(fnStyleTextarea($el, params))
            .add(fnStyleCheckbox($el, params))
            .add(fnStyleRadio($el, params))
            .add(fnStyleButton($el, params))
            .add(fnStyleSelect($el, params))
            .add(fnStyleFile($el, params));

        $el.add($result.addClass(params.clazz.el.wrap)).data('x-style-complete', true);
        $.xPreload(params.preload);
        return $result;
    };

    var fnGetType = function($el) {
            if ($el.is('input[type="text"], input[type="password"], input[type="checkbox"], input[type="radio"], input[type="file"]')) {
                return $el.attr('type');
            } else if ($el.is('a, button, input[type="button"], input[type="submit"], input[type="reset"]')) {
                return 'button';
            } else if ($el.is('textarea')) {
                return 'textarea';
            } else if ($el.is('select')) {
                return 'select';
            }
            return null;
        },

        fnReset = function($el, handler) {
            $el.closest('form').on('reset', function() {
                setTimeout(handler, 25);
            });
        },

        fnCreate = function($el, type, params) {
            var template = params[type].replace('#element#', '<span class="x-style-temp-el"></span>').replace('#scroll#', '<span class="x-style-temp-scroll"></span>'),
                $wrap = $(template).insertAfter($el).addClass($el.attr('class') || '');

            $wrap.find('.x-style-temp-el').replaceWith($el.detach());
            fnPlaceholder($wrap, $el, type, params);

            return $wrap;
        },

        fnScrollbar = function($el, $bar, params) {
            $bar.addClass(params.clazz.el.scroll);
            var $handle = $('<span></span>').addClass(params.clazz.el.handle).appendTo($bar),

                hChange = function() {
                    setTimeout(function() {
                        var sh = $el[0].scrollHeight,
                            ch = $el[0].clientHeight,
                            st = $el[0].scrollTop;

                        $bar.toggleClass(params.clazz.m.hide, sh <= ch);
                        $handle.css({ top: (st / (sh - ch) * 100) + '%' });
                    }, 25);
                },

                ev2scroll = function(ev) {
                    var t = $bar.offset().top + parseInt($bar.css('border-top'), 10) + parseInt($bar.css('padding-top'), 10),
                        h = $bar.height(),
                        pos = Math.max(0, Math.min(1, (ev.pageY - t) / h)),
                        sh = $el[0].scrollHeight,
                        ch = $el[0].clientHeight;

                    return Math.round((sh - ch) * pos / 20) * 20;
                },

                hDown = function(ev) {
                    $el.scrollTop(ev2scroll(ev));
                    hChange();
                    $('body').on('mousemove', hMove).on('mouseup', hUp);
                    ev.preventDefault();
                },

                hMove = function(ev) {
                    if (ev.which !== 0) {
                        $el.scrollTop(ev2scroll(ev));
                        hChange();
                    } else {
                        $('body').off('mousemove', hMove).off('mouseup', hUp);
                    }
                },

                hUp = function() {
                    $('body').off('mousemove', hMove).off('mouseup', hUp);
                };

            hChange();
            $el.on('change keydown', hChange);
            $bar.on('mousedown', hDown);
        },

        fnPlaceholder = function($wrap, $el, type, params) {
            if ($.inArray(type, ['text', 'password', 'textarea']) === -1 || $el.attr('placeholder') === undefined) { return; }

            var fnShow = function() {
                    if ($el.val() === '') {
                        $wrap.addClass(params.clazz.is.empty);
                        $ph.show();
                    }
                },

                fnHide = function() {
                    $wrap.removeClass(params.clazz.is.empty);
                    $ph.hide();
                },

                text = $el.attr('placeholder'),
                $ph;

            $el.removeAttr('placeholder');
            $ph = $('<span class="' + params.clazz.el.placeholder + '">' + text + '</span>').css({display: 'none'}).insertAfter($el);
            fnShow();

            $el.focus(fnHide).blur(fnShow);
            fnReset($el, function() {
                fnShow();
            });
        },

        fnHover = function($el, $w, params) {
            $el.on('mouseenter', function() {
                $w.addClass(params.clazz.is.hover);
            }).on('mouseleave', function() {
                $w.removeClass(params.clazz.is.hover);
            });
        },

        fnFocus = function($el, $w, params) {
            $el.focus(function() {
                $w.addClass(params.clazz.is.focus);
            }).blur(function() {
                $w.removeClass(params.clazz.is.focus);
            });
        },

        fnStyleText = function($list, params) {
            return $list.filter('input[type="text"]').map(function() {
                var $el = $(this),
                    $w = fnCreate($el, 'text', params);
                fnHover($w, $w, params);
                fnFocus($el, $w, params);
                return $w[0];
            });
        },

        fnStylePassword = function($list, params) {
            return $list.filter('input[type="password"]').map(function() {
                var $el = $(this),
                    $w = fnCreate($el, 'password', params);
                fnHover($w, $w, params);
                fnFocus($el, $w, params);
                return $w[0];
            });
        },

        fnStyleTextarea = function($list, params) {
            return $list.filter('textarea').map(function() {
                var $el = $(this),
                    $w = fnCreate($el, 'textarea', params),
                    $tScroll = $w.find('.x-style-temp-scroll');

                fnHover($w, $w, params);
                fnFocus($el, $w, params);

                if ($tScroll.length) {
                    var $bar = $('<div></div>');
                    $tScroll.after($bar).remove();
                    fnScrollbar($el, $bar, params);
                }

                return $w[0];
            });
        },

        fnStyleCheckbox = function($list, params) {
            return $list.filter('input[type="checkbox"]').map(function() {
                var $el = $(this),
                    $w = fnCreate($el, 'checkbox', params),

                    hChange = function() {
                        $w.toggleClass(params.clazz.is.active, $el[0].checked);
                    },

                    hClick = function(ev) {
                        if ($el.attr('disabled')) { return; }
                        if (ev === undefined || ev.target !== $el[0]) {
                            $el[0].checked = !$el[0].checked;
                            $el.trigger('change');
                        }
                    },

                    fnBindFocus = function() {
                        fnFocus($w, $w, params);
                        $w.on('click', function() {
                            if ($el.attr('disabled')) { return; }
                            $w.trigger('focus');
                        }).on('keydown', function(ev) {
                            if (ev.keyCode === 32) {
                                hClick();
                                ev.preventDefault();
                            }
                        });
                    };

                hChange();
                $el.on('change', hChange);

                $w.on('click', hClick).on('mousedown', function(e) {
                    e.preventDefault();
                });

                fnHover($w, $w, params);
                fnBindFocus();
                fnReset($el, hChange);

                return $w[0];
            });
        },

        fnStyleRadio = function($list, params) {
            return $list.filter('input[type="radio"]').map(function() {
                var $el = $(this),
                    $w = fnCreate($el, 'radio', params),
                    $group = $('input[type="radio"][name="' + $el.attr('name') + '"]'),

                    hChange = function() {
                        $w.toggleClass(params.clazz.is.active, $el[0].checked);
                    },

                    hClick = function(ev) {
                        if ($el.attr('disabled')) { return; }
                        if (ev === undefined || ev.target !== $el[0] && !$el[0].checked) {
                            $el[0].checked = true;
                            $group.trigger('change');
                        } else if (ev.target === $el[0]) {
                            $group.trigger('change');
                        }
                    },

                    fnBindFocus = function() {
                        fnFocus($w, $w, params);
                        $w.on('click', function() {
                            if ($el.attr('disabled')) { return; }
                            $w.trigger('focus');
                        }).on('keydown', function(ev) {
                            if (ev.keyCode === 32) {
                                hClick();
                                ev.preventDefault();
                            }
                        });
                    };

                hChange();
                $el.change(hChange);

                $w.on('click', hClick).on('mousedown', function(e) {
                    e.preventDefault();
                });

                fnHover($w, $w, params);
                fnBindFocus();
                fnReset($el, hChange);

                return $w[0];
            });
        },

        fnStyleButton = function($list, params) {
            return $list.filter('a, button, input[type="button"], input[type="submit"], input[type="reset"]').map(function() {
                var $el = $(this),
                    $w = fnCreate($el, 'button', params),
                    isInput = !$el.is('a, button'),

                    fnBindFocus = function() {
                        fnFocus($el, $w, params);
                        $el.on('click', function() {
                            $el.trigger('focus');
                        });
                    };

                $w.find('.' + params.clazz.el.title).html(isInput ? ($el.val() || '') : $el.html());
                fnHover($w, $w, params);
                fnBindFocus();

                return $w[0];
            });
        },

        fnStyleSelect = function($list, params) {
            return $list.filter('select').map(function() {
                var $el = $(this),
                    $options = $el.find('option'),
                    $w = fnCreate($el, 'select', params),
                    $t = $w.find('.' + params.clazz.el.title),
                    $drop = null,
                    mobile = $.xBrowser.platform === 'mobile',

                    fnInit = function() {
                        if (!$options.find('[selected]').length && $el.attr('placeholder')) {
                            $el[0].selectedIndex = -1;
                        }
                        hChange();
                    },

                    fnShow = function() {
                        if ($el.attr('disabled')) { return; }

                        $w.off('click', fnShow);
                        $drop = fnGetDrop().appendTo($w);
                        $w.addClass(params.clazz.is.active);

                        setTimeout(function() {
                            $('body').on('click', fnHide);
                        }, 100);
                    },

                    fnHide = function() {
                        $('body').off('click', fnHide);
                        $w.removeClass(params.clazz.is.active);
                        $drop.detach();
                        $w.on('click', fnShow);
                    },

                    hItemClick = function() {
                        var index = $(this).index();
                        if ($el[0].selectedIndex !== index) {
                            $el[0].selectedIndex = index;
                            $el.trigger('change');
                        }
                        $el.trigger('focus');
                    },

                    hChange = function() {
                        var ph = $el[0].selectedIndex === -1;
                        $t.html(ph ? $el.attr('placeholder') : $options.eq($el[0].selectedIndex).html());
                        $w.toggleClass(params.clazz.is.placeholder, ph);
                    },

                    fnGetDrop = function() {
                        var $liList = $options.map(function() {
                            return $(params.selectItem.replace('#title#', $(this).html())).toggleClass(params.clazz.is.disabled, !!this.disabled)[0];
                        });
                        $liList.eq($el[0].selectedIndex).addClass(params.clazz.is.active);

                        var $drop = (function() {
                            var $w = $(params.selectDrop.replace('#list#', '<span class="x-style-temp-el"></span>')).addClass($el.attr('class') || '');
                            $w.find('.x-style-temp-el').replaceWith($liList);
                            return $w;
                        }());

                        $drop.on('click', 'li', hItemClick);

                        return $drop;
                    },

                    fnBindFocus = function() {
                        fnFocus($el, $w, params);
                        $w.on('click', function() {
                            if ($el.attr('disabled')) { return; }
                            $el.trigger('focus');
                        });
                    };

                fnInit();
                $el.on('change', hChange);
                fnReset($el, hChange);

                if (mobile) {
                    $w.addClass(params.clazz.m.mobile);
                } else {
                    $w.on('click', fnShow).on('mousedown', function(e) {
                        e.preventDefault();
                    });
                    fnHover($w, $w, params);
                    fnBindFocus();
                }
            });
        },

        fnStyleFile = function($list, params) {
            return $list.filter('input[type="file"]').map(function() {
                var $el = $(this),
                    $w = fnCreate($el, 'file', params),
                    $h = $w.find('.' + params.clazz.el.holder),
                    ph = $el.attr('placeholder') || '',
                    $t = $w.find('.' + params.clazz.el.title),

                    fnUpdate = function() {
                        var str = $el.val().replace(/^.+?[\/\\]([^\/\\]+?)(?:\.([^\/\\]+))?$/, params.fileFormat.replace('$name', '$1').replace('$ext', '$2'));
                        $t.html(str !== '' ? str : ph);
                        $w.toggleClass(params.clazz.is.active, str !== '');
                    },

                    fnBindFocus = function() {
                        fnFocus($el, $w, params);
                        $h.on('click', function() {
                            $el.trigger('focus');
                        });
                    };

                $el.removeAttr('placeholder');
                fnUpdate();

                $el.on('change', fnUpdate);
                fnHover($h, $w, params);
                fnBindFocus();
                fnReset($el, fnUpdate);
            });
        };
}(jQuery));
/* End Style **************************************************************************************************************************************************/



/* Validator **************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.fn.xValidator = function(params) {
        return this.each(function() {
            var $el = $(this);
            if (!$el.data('x-validator')) {
                $el.data('x-validator', new Validator($el, params));
            }
        });
    };

    var Validator = function(selector, params) {
        this._initialize(selector, params);
        this._bindHandlers();
        this.check();
    };

    $.extend(Validator.prototype, {

        _initialize: function(selector, params) {
            this.$form = $(selector);

            $.extend(this, {
                url:      this.$form.attr('js-action-validate'),
                sFields:  'input, textarea, select',
                timeout:  500,
                cbChange: $.noop,
                cValid:   'm-valid',
                cInvalid: 'm-invalid'
            }, params, {
                aTouched: []
            });

            $.xHelpers.selector2object(this, this.$form);
        },

        _bindHandlers: function() {
            var _this = this;

            this.$fields.on('keydown change', function() {
                _this.check();
            });

            this.$form.one('submit', function() {
                _this.touch(_this.$fields);
            }).on('submit', $.proxy(this.hSubmit, this));
        },

        hSubmit: function(ev) {
            if (!ev.isTrigger) {
                this.check().done($.proxy(function(count) {
                    if (!count) { this.$form.submit(); }
                }, this));
                ev.preventDefault();
            }
        },

        touch: function(selector) {
            var oName = {};

            $.each(this.aTouched, function() {
                oName[this] = true;
            });
            $(selector).each(function() {
                oName[$(this).attr('name')] = true;
            });

            this.aTouched = [];
            $.each(oName, $.proxy(function(i) {
                this.aTouched.push(i);
            }, this));
        },

        check: (function() {
            var idTimeout = null,
                dComplete = null,
                idRequestLast = 0;

            return function() {
                var _this = this,
                    idRequestCurrent = ++idRequestLast;

                if (dComplete === null) {
                    dComplete = $.Deferred();
                }

                clearTimeout(idTimeout);
                idTimeout = setTimeout(function() {
                    idTimeout = null;
                    $.ajax({
                        url: _this.url,
                        data: _this.$form.serialize(),
                        type: 'POST',
                        dataType: 'json',
                        success: function(aName) {
                            if (idRequestCurrent !== idRequestLast) { return; }
                            var count = aName ? aName.length : 0;
                            _this._update(aName);
                            _this.$form.toggleClass(_this.cInvalid, !!count).toggleClass(_this.cValid, !count);
                            dComplete.resolve(count);
                            dComplete = null;
                        }
                    });
                }, this.timeout);

                return dComplete;
            };
        }()),

        _update: function(aName) {
            $.each(this.$fields, function(el) {
                var $el = $(el),
                    name = $el.attr('name'),

                    wasInvalid = $el.hasClass(this.cInvalid),
                    isInvalid = $.inArray(name, aName) !== -1,
                    isTouched = $.inArray(name, this.aTouched) !== -1;

                if (isTouched && ((!wasInvalid && isInvalid) || (wasInvalid && !isInvalid))) {
                    $el.toggleClass(this.cInvalid, isInvalid).toggleClass(this.cValid, !isInvalid);
                    this.cbChange($el, name, isInvalid);
                }
            }, this);
        }

    });
}(jQuery));
/* End Validator **********************************************************************************************************************************************/



/* Slider *****************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.fn.xSlider = function(params) {
        return this.each(function() {
            var $el = $(this);
            if ($el.data('x-slider')) { return; }
            $el.data('x-slider', new XSlider($el, params));
        });
    };



    var XSlider = function(selector, params) {
        this._initialize(selector, params);
        this._update();
        this.$items.hide().eq(this.index).show();
        this._bindHandlers();
    };

    XSlider.prototype = {

        _initialize: function(selector, params) {
            this.$slider = $(selector);

            $.extend(this, {
                index:    0,
                duration: 500,
                auto:     false,
                cycle:    false,
                sWrap:    '> .r-list',
                sItems:   '> .r-list > li',
                sButtons: '> .r-buttons > li',
                sPrev:    '> .e-arrow.n-prev',
                sNext:    '> .e-arrow.n-next'
            }, params, {
                busy:     false
            });

            $.xHelpers.selector2object(this, this.$slider);
        },

        _bindHandlers: function() {
            this.$prev.on('click', $.proxy(this.prev, this));
            this.$next.on('click', $.proxy(this.next, this));
            this.$buttons.on('click', $.proxy(function() {
                this.set(this.$buttons.index(this));
            }, this));
            this.$buttons.add(this.$prev).add(this.$next).on('mousedown', function(ev) {
                ev.preventDefault();
            });
        },

        _update: function(index) {
            index = index !== undefined ? index : this.index;
            this.$buttons.removeClass('is-active').eq(index).addClass('is-active');
            this.$prev.toggleClass('m-disabled', !this.cycle && index === 0);
            this.$next.toggleClass('m-disabled', !this.cycle && index === this.$items.length - 1);
        },

        _change: function(index, newIndex) {
            var _this = this,
                dComplete = $.Deferred();

            this.$items.eq(newIndex).css({ zIndex: 1 }).fadeIn(this.duration, function() {
                _this.$items.eq(index).hide();
                $(this).css({ zIndex: '' });
                dComplete.resolve();
            });

            return dComplete;
        },

        prev: function() {
            if (this.cycle || this.index > 0) {
                this.set((this.$items.length + this.index - 1) % this.$items.length);
            }
        },

        next: function() {
            if (this.cycle || this.index < this.$items.length - 1) {
                this.set((this.index + 1) % this.$items.length);
            }
        },

        set: function(newIndex) {
            if (this.busy || newIndex === this.index || newIndex < 0 || newIndex >= this.$items.length) { return; }
            this.busy = true;

            this._update(newIndex);
            $.when(this._change(this.index, newIndex)).done($.proxy(function() {
                this.index = newIndex;
                this.busy = false;
            }, this));
        }
    };
}(jQuery));
/* End Slider *************************************************************************************************************************************************/



/* Tabs *******************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.fn.xTabs = function(params) {
        return this.each(function() {
            var $el = $(this);
            if ($el.data('x-tabs')) { return; }
            $el.data('x-tabs', new XTabs($el, params));
        });
    };

    $.xTabs = function(sButtons, sItems, params) {
        return new XTabs(undefined, $.extend(params, {
            $buttons: $(sButtons),
            $items: $(sItems)
        }));
    };



    var XTabs = function(selector, params) {
        this._initialize(selector, params);
        this._update();
        this.$items.hide().eq(this.index).show();
        this._bindHandlers();
    };

    XTabs.prototype = {

        _initialize: function(selector, params) {
            this.$tabs = $(selector);

            $.extend(this, {
                index:    0,
                duration: 500,
                easing:   undefined,
                hAnimate: false,
                sWrap:    '> .r-list',
                sItems:   '> .r-list > li',
                sButtons: '> .r-buttons > li'
            }, params, {
                busy:        false,
                aProperties: [ 'marginTop', 'borderTopWidth', 'paddingTop', 'height', 'paddingBottom', 'borderBottomWidth', 'marginBottom' ]
            });

            $.xHelpers.selector2object(this, this.$tabs);
        },

        _bindHandlers: function() {
            this.$buttons
                .on('click', $.proxy(function(ev) {
                    this.set(this.$buttons.index(ev.currentTarget));
                }, this))
                .on('mousedown', function(ev) {
                    ev.preventDefault();
                });
        },

        set: function(newIndex) {
            if (this.busy || newIndex === this.index || newIndex < 0 || newIndex > this.$items.length) { return; }
            this.busy = true;

            $.when(
                this._animateHeight(newIndex),
                this._change(this.index, newIndex)
            ).done($.proxy(function() {
                this.index = newIndex;
                this.busy = false;
            }, this));
        },

        _update: function(index) {
            index = index !== undefined ? index : this.index;
            this.$buttons.removeClass('is-active').eq(index).addClass('is-active');
        },

        _getItemHeight: function(index) {
            var height = 0,
                $el = this.$items.eq(index),
                display = $el.css('display');

            $el.css({ display: 'block' });
            $.each(this.aProperties, function(i, value) {
                height += parseInt($el.css(value), 10);
            });

            $el.css({ display: display });
            return height;
        },

        _animateHeight: function(index) {
            if (!this.hAnimate) { return null; }

            var dComplete = $.Deferred();
            this.$wrap.animate({ height: this._getItemHeight(index) }, this.duration, this.easing, function() {
                $(this).css({ height: '' });
                dComplete.resolve();
            });

            return dComplete;
        },

        _change: function(index, newIndex) {
            var _this = this,
                dComplete = $.Deferred();

            this.$items.eq(index).fadeTo(this.duration / 2, 0, function() {
                !_this.hAnimate && _this.$wrap.height(_this.$wrap.height());
                $(this).css({ opacity: '', display: 'none' });
                _this.$items.eq(newIndex).fadeIn(_this.duration / 2, function() {
                    dComplete.resolve();
                });
                !_this.hAnimate && _this.$wrap.css({ height: '' });
            });

            return dComplete;
        }
    };
}(jQuery));
/* End Tabs ***************************************************************************************************************************************************/



/* Spoiler ****************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.xSpoiler = function(sHandle, sContent, params) {
        return new XSpoiler(sHandle, sContent, params);
    };

    $.fn.xSpoiler = function(params) {
        return this.each(function() {
            var $el = $(this);
            if ($el.data('x-spoiler')) { return; }
            $el.data('x-spoiler', new XSpoiler($el, $($el.attr('href')), params));
        });
    };

    $.fn.xSpoilerList = function(params) {
        return this.filter('dl').each(function() {
            var $el = $(this);
            if ($el.data('x-spoiler-list')) { return; }
            $el.data('x-spoiler-list', true);

            $el.find('dt').each(function() {
                $.xSpoiler(this, $(this).next('dd'), params);
            });
        });
    };



    var XSpoiler = function(sHandle, sContent, params) {
        this._initialize(sHandle, sContent, params);
        this.$content.css({ display: this.isOpen ? 'block' : 'none' });
        this._bindHandlers();
    };

    XSpoiler.prototype = new $.xHelpers.XToggleContent();

    $.extend(XSpoiler.prototype, {

        _initialize: function(sHandle, sContent, params) {
            $.extend(this, {
                expDuration: true,
                easing:      undefined
            }, params, {
                busy:        false,
                $handle:     $(sHandle),
                $content:    $(sContent),
                $body:       $('body'),
                aProperties: [ 'marginTop', 'borderTopWidth', 'paddingTop', 'height', 'paddingBottom', 'borderBottomWidth', 'marginBottom' ]
            });

            this.isOpenNext = this.isOpen;
            $.xHelpers.selector2object(this, this.$content);
        },

        _change: function() {
            var _this = this,
                oProperties = this._getProperties(),
                duration = !this.expDuration ? this.duration : Math.pow(oProperties.height, 0.32) * this.duration * 1.5 * 0.1,
                dComplete = $.Deferred();

            if (this.isOpenNext) {
                this.$content.css($.xObject(this.aProperties, 0)).css({ display: 'block' }).animate(oProperties, duration, this.easing, function() {
                    $(this).css($.xObject(_this.aProperties, ''));
                    dComplete.resolve();
                });
            } else {
                this.$content.animate($.xObject(this.aProperties, 0), duration, this.easing, function() {
                    $(this).css({ display: 'none' }).css($.xObject(_this.aProperties, ''));
                    dComplete.resolve();
                });
            }

            return dComplete;
        },

        _getProperties: function() {
            var _this = this,
                display = this.$content.css('display'),
                oProperties = {};

            this.$content.css({ display: 'block' });
            $.each(this.aProperties, function(i, value) {
                oProperties[value] = parseInt(_this.$content.css(value), 10);
            });

            this.$content.css({ display: display });
            return oProperties;
        }
    });
}(jQuery));
/* End Spoiler ************************************************************************************************************************************************/



/* Popup ******************************************************************************************************************************************************/
(function($) {
    'use strict';

    $.xPopup = function(sHandle, sContent, params) {
        return new XPopup(sHandle, sContent, params);
    };

    $.fn.xPopup = function(params) {
        return this.each(function() {
            var $el = $(this);
            if ($el.data('x-popup')) { return; }
            $el.data('x-popup', new XPopup($el, $($el.attr('href')), params));
        });
    };



    var XPopup = function(sHandle, sContent, params) {
        this._initialize(sHandle, sContent, params);
        this.$content.css({ display: this.isOpen ? 'block' : 'none' });
        this._bindHandlers();
    };

    XPopup.prototype = new $.xHelpers.XToggleContent();

    $.extend(XPopup.prototype, {

        _initialize: function(sHandle, sContent, params) {
            $.extend(this, {
                duration: 300,
                bodyClose: true
            }, params, {
                busy:        false,
                $handle:     $(sHandle),
                $content:    $(sContent),
                $body:       $('body')
            });

            this.isOpenNext = this.isOpen;
            $.xHelpers.selector2object(this, this.$content);
        },

        _change: function() {
            var dComplete = $.Deferred();
            this.$content.fadeToggle(this.duration, function() {
                dComplete.resolve();
            });
            return dComplete;
        }
    });
}(jQuery));
/* End Popup **************************************************************************************************************************************************/



/* Modal ******************************************************************************************************************************************************/
(function($) {
    'use strict';

    var xModal = null;

    $.xModal = {
        open: function(data, params) {
            if (!xModal || !xModal.isOpen) {
                xModal = new XModal(params);
                xModal.open(data);
            }
        },

        reopen: function(data) {
            if (xModal && xModal.isOpen) {
                xModal.reopen(data);
            }
        },

        close: function() {
            if (xModal && xModal.isOpen) {
                xModal.close();
            }
        }
    };

    $.fn.xModal = function(params) {
        return this.each(function() {
            var $a = $(this);
            if ($a.data('x-modal-gallery')) { return; }
            $a.data('x-modal-gallery', true);

            $a.on('click', function(ev) {
                $.xModal.open($a.attr('href'), params);
                ev.preventDefault();
            });
        });
    };

    $.fn.xModalGallery = function(aImageSrc, pGallery, pModal) {
        pGallery = $.extend({
            start: 0,
            $prev: $('<a class="xm-arrow n-prev"></a>'),
            $next: $('<a class="xm-arrow n-next"></a>')
        }, pGallery);

        return this.each(function() {
            var $a = $(this);
            if ($a.data('x-modal-gallery')) { return; }
            $a.data('x-modal-gallery', true);

            var index = null,
                isOpen = false,

                fnOpen = function(ev) {
                    if (isOpen) { return; }
                    isOpen = true;

                    index = pGallery.cbStart;
                    $.xPreload(aImageSrc[index]).done(function() {
                        $.xModalOpen('<img src="' + aImageSrc[index] + '">', $.extend({}, pModal, {
                            cbBeforeShow: function(oModal) {
                                oModal.$content.append(pGallery.$prev, pGallery.$next);
                                (pModal.cbBeforeShow || $.noop)(oModal);
                            }
                        }));
                    });

                    ev.preventDefault();
                },

                fnPrev = function() {
                    if (isOpen) {
                        index = (aImageSrc.length + index - 1) % aImageSrc.length;
                        $.xPreload(aImageSrc[index]).done(function() {
                            $.xModalReopen('<img src="' + aImageSrc[index] + '">');
                        });
                    }
                },

                fnNext = function() {
                    if (isOpen) {
                        index = (index + 1) % aImageSrc.length;
                        $.xPreload(aImageSrc[index]).done(function() {
                            $.xModalReopen('<img src="' + aImageSrc[index] + '">');
                        });
                    }
                };

            $a.on('click', fnOpen);
            pGallery.$prev.on('click', fnPrev);
            pGallery.$next.on('click', fnNext);
        });
    };



    var XModal = function(params) {
        $.extend(this, {
            duration:       300,
            overlayOpacity: 0.7,
            overlayClose:   true,
            buttonClose:    true,
            escClose:       true,
            $ph:            $('<div id="xm-placeholder"></div>'),
            $content:       $('<div id="xm-content"></div>'),
            $wrapper:       $('<div id="xm-content-wrapper"></div>'),
            $overlay:       $('<div id="xm-overlay"></div>'),
            $container:     $('<div id="xm-container"></div>'),
            $close:         $('<a class="xm-close"></a>'),
            $fixed:         $([]),
            cbOnShow:       $.noop,
            cbAfterHide:    $.noop
        }, params, {
            $wnd:      $(window),
            $body:     $('body'),
            $doc:      $(document),
            $data:     null,
            isURL:     null,
            wScroll:   0,
            tScroll:   0,
            isOpen:    false
        });

        this.$content.css({ display: 'none' });
        this.$overlay.css({ display: 'none' });

        this.$container.append(this.$wrapper.append(this.$content), this.$overlay);
    };

    XModal.prototype = {

        open: function(data) {
            if (this.isOpen) {
                return;
            }

            this._enableScrollHack();
            this._hWndResize();

            this.$container.addClass('fx');
            this.$body.addClass('x-modal-open').append(this.$container);

            this._showOverlay($.proxy(function() {
                this.buttonClose && this.$content.prepend(this.$close);
                this._loadContent(data, $.proxy(function() {
                    this._showContent($.proxy(function() {
                        this.$container.removeClass('fx');
                        this._bindEventHandlers();
                        this.isOpen = true;
                    }, this));
                    this.cbOnShow(this);
                }, this));
            }, this));
        },

        reopen: function(data) {
            if (!this.isOpen) {
                return;
            }
            this.$container.addClass('fx');

            this._hideContent($.proxy(function() {
                this.cbAfterHide(this);
                this.$content.empty();
                this.buttonClose && this.$content.prepend(this.$close);
                this._loadContent(data, $.proxy(function() {
                    this._showContent($.proxy(function() {
                        this.$container.removeClass('fx');
                    }, this));
                    this.cbOnShow(this);
                }, this));
            }, this));
        },

        close: function() {
            if (!this.isOpen) {
                return;
            }
            this.isOpen = false;
            this._unbindEventHandlers();

            this.$container.addClass('fx');
            this._hideContent($.proxy(function() {
                this.cbAfterHide(this);
                this._hideOverlay($.proxy(function() {
                    this.$container.removeClass('fx');
                    this._unloadContent();
                    this._disableScrollHack();
                }, this));
            }, this));
        },

        _hEscClose: function(ev) {
            if (ev.which === 27) {
                this.close();
            }
        },

        _hWndResize: function() {
            var mr = this.$doc.height() > this.$body.height() ? this.wScroll : '';
            this.$body.add(this.$fixed).css({ marginRight: mr });

            this.$wrapper.css({
                width: this.$body.width(),
                height: this.$body.height()
            });

            this.$overlay.css({ marginRight: this.wScroll });
        },

        _hWndScroll: function() {
            var l = this.$wnd.scrollLeft();
            $('#xm-container').css({
                left: -l,
                right: l
            });
            this.$wnd.scrollTop(this.tScroll);
        },

        _fnOverlayClose: function(ev) {
            if (ev.target === this.$wrapper[0]) {
                this.close();
            }
        },

        _enableScrollHack: function() {
            this.wScroll = -this.$body.css({ overflow: 'scroll' }).width() + this.$body.css({ overflow: 'hidden' }).width();
            //this.$container.css({ paddingRight: this.wScroll });
        },

        _disableScrollHack: function() {
            this.$body.removeClass('x-modal-open').css({ marginRight: '', overflow: '' });
            this.$fixed.css({ marginRight: '' });
        },

        _bindEventHandlers: function() {
            this.tScroll = this.$wnd.scrollTop();
            this.$wnd
                .on('resize', $.proxy(this._hWndResize, this))
                .on('scroll', $.proxy(this._hWndScroll, this));
            if (this.overlayClose) {
                this.$body.on('click', $.proxy(this._fnOverlayClose, this));
            }
            if (this.buttonClose) {
                this.$close.on('click', $.proxy(this.close, this));
            }
            if (this.escClose) {
                this.$body.on('keydown', $.proxy(this._hEscClose, this));
            }
        },

        _unbindEventHandlers: function() {
            $(window).off('resize', this._hWndResize).off('scroll', this._hWndScroll);
            this.$body.off('click', this._fnOverlayClose).off('keydown', this._hEscClose);
            this.$close.off('click', this.close);
        },

        _showOverlay: function(cb) {
            this.$overlay
                .css({ display: 'block', opacity: 0 })
                .fadeTo(this.duration, this.overlayOpacity, cb || $.noop);
        },

        _hideOverlay: function(cb) {
            this.$overlay.fadeOut(this.duration, cb || $.noop);
        },

        _loadContent: function(data, cb) {
            this.isURL = $.type(data) === 'string';
            if (this.isURL) {
                var url = /^\//.test(data) ? data : window.location.href.replace(/^(.+\/).*$/, '$1') + data;
                $.get(url, $.proxy(function(html) {
                    if (/\.(png|jpg|jpeg|gif|bmp)$/.test(data)) {
                        html = '<img src="' + data + '">';
                    }
                    this.$data = $(html);
                    this.$data.appendTo(this.$content);
                    (cb || $.noop)();
                }, this));
            } else {
                this.$data = data;
                this.$data.after(this.$ph).detach().appendTo(this.$content);
                (cb || $.noop)();
            }
        },

        _unloadContent: function() {
            this.$container.detach();
            if (this.isURL) {
                this.$data.detach();
            } else {
                this.$ph.before(this.$data.detach()).detach();
            }
        },

        _showContent: function(cb) {
            this.$content.fadeIn(this.duration, cb || $.noop);
        },

        _hideContent: function(cb) {
            this.$content.fadeOut(this.duration, cb || $.noop);
        }
    };
}(jQuery));
/* End Modal **************************************************************************************************************************************************/
