<?php
    ob_start(); // Initiate the output buffer
?>
<html lang="ru"
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html" />
    <meta name="author" content="" />
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, user-scalable=0" />

	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	
	<link rel="stylesheet" href="/css/style.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.bundle.min.js"></script>
    <title><?php echo getLang("ADMIN_PANEL_1");?></title>
</head>

<body>
<header> 

</header>

<div class="container_menu">

	<div class="member_avatar text-center">
        <div class="row">
            <div class="col-md-9">
                <p><?php echo GetUserNameByEmail($_COOKIE['info1']); ?></p>
            </div>
            <div class="col-md-3">
                <div class="btn-group">
                  <button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <?php echo getLang("MENU_1");?>
                  </button>
                  <div class="dropdown-menu">
                      <a class="dropdown-item" href="http://<?php echo $_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI'];?>/language/0/">RU</a>
                      <a class="dropdown-item" href="http://<?php echo $_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI'];?>/language/1/">EN</a>
                  </div>
                </div>
            </div>
        </div>
	</div>
	
	<div class=" member_avatar text-center">
	  <img src="/images/user/<?php echo GetUserAvatar(GetUserIDByEmail($_COOKIE["info1"]));?>" class="rounded-circle" alt="...">
	</div>
	<div class="main_buttons row">

	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
		<button type="button" class="btn btn-success" onclick="location.href='http://<?php echo $_SERVER["HTTP_HOST"];?>/admin/setting'"><?php echo getLang("MENU_2");?></button>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
		<button type="button" class="btn btn-success" onclick="location.href='http://<?php echo $_SERVER["HTTP_HOST"];?>/admin/exit'"><?php echo getLang("MENU_3");?></button>
	</div>

	</div>
	<div class="main_menu">
		<ul>
            <li><button type="button" class="btn btn-light" onclick="location.href='http://<?php echo $_SERVER["HTTP_HOST"];?>/admin/list/1'"><?php echo getLang("BUTTON_1");?></button></li>
            <li><button type="button" class="btn btn-light" onclick="location.href='http://<?php echo $_SERVER["HTTP_HOST"];?>/admin/list/2'"><?php echo getLang("BUTTON_2");?></button></li>
            <?php // загрузка всех активных плагинов (use=1) в виде кнопок в менюхе
                echo ButtonController::GetButtonText();
            ?>
		</ul>
	</div>
</div>
<div class="container_content">
	<div class="container">
	<p>
        <?php
            for($i=1;$i<5;$i++){
                switch($i){// прогрузка ключей в url
                    case 1:{$golistinfo=GetInfoURL("language");if($golistinfo==0)break(2);}
                    case 2:{$golistinfo=GetInfoURL("setting");if($golistinfo==0)break(2);}
                    case 3:{$golistinfo=GetInfoURL("list");if($golistinfo==0)break(2);}
                    case 4:{$golistinfo=GetInfoURL("exit");if($golistinfo==0)break(2);}
                }
            }
            if($golistinfo==1){
                if(strlen($url[1]))header('Location:http://' .$_SERVER["HTTP_HOST"]."/admin");
                else{
                    echo "Здесь должна быть Главная страница, но ее нет! 2";
                }
            }
        ?>
    </p>
	</div>
</div>

<footer>

</footer>
        <script type="text/javascript">
            jQuery( function($) {
                // $('tr:even').css({'background-color' : '#d9d8df'});
                $('tbody tr[data-href]').addClass('clickable').click( function() {
                    window.location = $(this).attr('data-href');
                }).find('a').hover( function() {
                    $(this).parents('tr').unbind('click');
                }, function() {
                    $(this).parents('tr').click( function() {
                        window.location = $(this).attr('data-href');
                    });
                });
            });
        </script>
        <style type="text/css">
            .movelink { cursor: pointer; }
        </style>


        <script>
            $('#img').change(function () {
                var input = $(this)[0];
                if (input.files && input.files[0]) {
                    if (input.files[0].type.match('image.*')) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#img-preview').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    } else {
                        console.log('ошибка, не изображение');
                    }
                } else {
                    console.log('ошибка изображения');
                }
            });

            $('#reset-img-preview').click(function() {
                $('#img').val('');
                $('#img-preview').attr('src', 'no_avatar_m.png');
            });

            $('#form').bind('reset', function () {
                $('#img-preview').attr('src', 'no_avatar_m.png');
            });
        </script>
</body>
</html>

<?php
    ob_end_flush(); // Flush the output from the buffer
?>