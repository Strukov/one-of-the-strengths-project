<?php

class AccountModel extends Core
{
    static function GetAccountModel($max_list=1,$filter="")
    {
        $usefilter="";
        if(strlen($filter))$usefilter="WHERE `login` LIKE '%".$filter."%' OR `email` LIKE '%".$filter."%'";
        $result = Core::ConnectBD("SELECT * FROM `user` ".$usefilter."LIMIT ".(($max_list-1)*MAX_DISPLAY_LIST).",".(MAX_DISPLAY_LIST+1));
        $data=1;
        $ss=array();
        if($result) {
            $rows = mysqli_num_rows($result);
            if($rows>MAX_DISPLAY_LIST)$max=$rows-1;
            else $max=$rows;
            for ($i = 1; $i <= $max; $i++) {
                $json=array("result".$i=>(mysqli_fetch_assoc($result)));
                $ss = $ss+($json);
                $data+=1;
            }
            $ss= $ss+(array("maxuser"=>$rows,"maxdata"=>--$data));
        }
        return json_encode($ss);
    }
}
