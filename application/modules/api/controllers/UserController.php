<?php

class UserController extends UserModel
{
    static function GetUserData($user_id=0)
    {
        $obj = json_decode(UserModel::GetUserModel($user_id));
        $userImages=$obj->{'images'};
        if(strlen($userImages)<=0||!file_exists("./images/user/".$userImages)){
            if($obj->{'sex'}==1)
                $userImages="no_avatar_j.png";
            else $userImages="no_avatar_m.png";
        }
        if($obj->{'active'}==1)
            $statusText="Активный";
        else if($obj->{'archive'}==1)
            $statusText="В архиве";
        else if($obj->{'deleted'}==1)
            $statusText="Удален";
        else
            $statusText="Не подтвержден";
        $textresult="
            <p>Аккаунт: ".$obj->{'login'}."</p>
            <img src=\"/images/user/".$userImages."\" class=\"rounded-circle\" width=\"200\" height=\"200\" alt=\"images...\">
            <p>Email: ".$obj->{'email'}."</p>
            <p>reg_ip: ".$obj->{'reg_ip'}."</p>
            <p>use_ip: ".$obj->{'use_ip'}."</p>
            <p>created_at: ".$obj->{'created_at'}."</p>
            <p>last_login_at: ".$obj->{'last_login_at'}."</p>
            <p>modified_at: ".$obj->{'modified_at'}."</p>
            <p>modified_by: ".GetUserNameByID($obj->{'modified_by'})."</p>
            <p>status account: ".$statusText."</p>
        ";
        if(GetAdminLevel($_COOKIE['info4'])>0)$textresult.="
            <button type=\"button\" class=\"btn btn-primary\" onclick=\"location.href='edit'\">Редактировать аккаунт</button>
        ";
        return $textresult;
    }

    static function AddUserData(){
        $sexinfo1="";
        $infotexterror="";
        if($_SESSION['RegUserError1']==1){$setErrorColor1="has-error";$infotexterror.="<span style=\"background:#FFCCCC\">Ник пользователя занят!<br></span>";}
        if($_SESSION['RegUserError2']==1){$setErrorColor2="has-error";$infotexterror.="<span style=\"background:#FFCCCC\">Почтовый ящик занят!<br></span>";}
        if($_SESSION['RegUserError3']==1){$setErrorColor3="has-error";$infotexterror.="<span style=\"background:#FFCCCC\">Вы не ввели пароль!<br></span>";}
        if($_SESSION['RegUserError4']==1){$setErrorColor4="has-error";$infotexterror.="<span style=\"background:#FFCCCC\">Вы не выбрали Ваш пол!</span>";}
        unset($_SESSION['RegUserError1']);
        unset($_SESSION['RegUserError2']);
        $addresult="
            <b>Регистрация нового пользователя</b>
            <br>
            <span>Чтобы пользоватся всеми возможностями сервиса, необходимо создать учетную запись.</span>
            <br>
            ".$infotexterror."
            <br>
            <form method=\"POST\" action=\"/ControllerMethod.php\" class=\"md-form my_form\">
            <div class=\"form-group ".$setErrorColor1."\">
                <label for=\"inputdefault\">Ник пользователя:</label>
                <input class=\"form-control\" id=\"inputdefault\" type=\"text\" name=\"index1\" value='".$_SESSION['RegUser1']."'>
            </div>
            <div class=\"form-group ".$setErrorColor2."\">
                <label for=\"inputdefault\">Почтовый ящик:</label>
                <input class=\"form-control\" id=\"inputdefault\" type=\"text\" name=\"index2\" value='".$_SESSION['RegUser2']."'>
            </div>
            <div class=\"form-group ".$setErrorColor3."\">
                <label for=\"inputdefault\">Пароль:</label>
                <input class=\"form-control\" id=\"inputdefault\" type=\"text\" name=\"index3\">
            </div>
            <label class=\"control-label col-xs-1\">Пол: </label>
                <label class=\"radio-inline\">
                    <input type=\"radio\" name=\"index4\" value='0'>Мужской
                </label>
                <label class=\"radio-inline\">
                    <input type=\"radio\" name=\"index4\" value='1'>Женский
                </label>
            <br>
            <br>
            <div class=\"form-group\">
                <input type=\"submit\" name=\"AddUser\" class=\"btn btn-primary\" value=\"Дальше\">
            </div>
        </form>
        ";
        return $addresult;
    }

    static function СonfirmationUserData(){
        $сonfresult="
            <b>Подтверждение пользователя</b>
            <br>
            <span>
                Второй этап регистрации, также не менее важный шаг!<br>
                Введите код подтверждения регистрации, который пришел на указанную Вами почту ''
            </span>
            <form method=\"POST\" action=\"/ControllerMethod.php\" class=\"md-form my_form\">
                <div class=\"form-group\">
                    <label for=\"inputdefault\">Код подтверждения (".$_SESSION['RegUserToken']."):</label>
                    <input class=\"form-control\" id=\"inputdefault\" type=\"text\" name=\"index1\"'>
                </div>
                <br>
                <br>
                <div class=\"form-group\">
                    <input type=\"submit\" name=\"СonfirmationUser\" class=\"btn btn-primary\" value=\"Подтвердить\">
                </div>
            </form>
        ";
        return $сonfresult;
    }

    static function SuccessСonfirmationUserData(){
        $susconfresult="
            <p>Аккаунт успешно подтвержден!</p>
            <br>
        ";
        return $susconfresult;
    }

    static function EditUserData($user_id=0){
        $obj = json_decode(UserModel::GetUserModel($user_id));
        $userImages=$obj->{'images'};
        if(strlen($userImages)<=0||!file_exists("./images/user/".$userImages)){
            if($obj->{'sex'}==1)
                $userImages="no_avatar_j.png";
            else $userImages="no_avatar_m.png";
        }
        if($obj->{'sex'}==0)$checkedSex1="checked";
        if($obj->{'sex'}==1)$checkedSex2="checked";
        $admin=GetAdminLevel($obj->{'id'});
        for($i=0;$i<8;$i++){
            if($admin==$i)$checkedAdmin[$i]="selected";
        }
        if($obj->{'active'}==1)
            $checkedStatus[1]="selected";
        else if($obj->{'archive'}==1)
            $checkedStatus[2]="selected";
        else if($obj->{'deleted'}==1)
            $checkedStatus[3]="selected";
        else
            $checkedStatus[0]="selected";
        if($_SESSION['noCorrectPass']===1){
            $norepass="has-error";
            $norepass2="<span style=\"background:#FFCCCC\">Вы повторили некорректный пароль! </span>";
            unset($_SESSION['noCorrectPass']);
        }
        if(AdminLevelAccess(7,GetAdminLevel($_COOKIE['info4'])))$editadminlevel="
            <div class=\"form-group col-xs-12\">
                <label class=\"control-label col-xs-3\">Уровень администратора:</label>
                <div class=\"col-xs-3\">
                  <select class=\"form-control\" name=\"index5\">
                    <option value='0' ".$checkedAdmin[0].">Без админки</option>
                    <option value='1' ".$checkedAdmin[1].">Администратор 1 лвл</option>
                    <option value='2' ".$checkedAdmin[2].">Администратор 2 лвл</option>
                    <option value='3' ".$checkedAdmin[3].">Администратор 3 лвл</option>
                    <option value='4' ".$checkedAdmin[4].">Администратор 4 лвл</option>
                    <option value='5' ".$checkedAdmin[5].">Администратор 5 лвл</option>
                    <option value='6' ".$checkedAdmin[6].">Администратор 6 лвл</option>
                    <option value='7' ".$checkedAdmin[7].">Администратор 7 лвл</option>
                  </select>
                </div>
            </div>
        ";
        $textresult="
        <form method=\"POST\" action=\"/ControllerMethod.php\" class=\"md-form my_form\" enctype=\"multipart/form-data\">
            <input type=\"hidden\" name=\"index1\" value=\"".$obj->{'id'}."\">
            
            <img id=\"img-preview\" width=\"200\" height=\"200\" src=\"/images/user/".$userImages."\" class=\"rounded-circle\" alt=\"images...\">
            <input type=\"file\" id=\"img\" multiple accept=\"image/*\" name=\"pictures[]\"/>
           
            <br>
            <br>
            <div class=\"input-group col-xs-4\">
              <span class=\"input-group-addon\">Nick</span>
              <input id=\"msg\" type=\"text\" class=\"form-control\" name=\"index2\" value=\"".$obj->{'login'}."\">
            </div>
            <br>
            <div class=\"input-group col-xs-4\">
              <span class=\"input-group-addon\">Email</span>
              <input id=\"msg\" type=\"text\" class=\"form-control\" name=\"index3\" value=\"".$obj->{'email'}."\">
            </div>
            <br>
            <label class=\"control-label col-xs-1\" for=\"inputEmail\">Пол: </label>
            <label class=\"radio-inline\">
                <input type=\"radio\" name=\"index4\" value='0' ".$checkedSex1.">Мужской
            </label>
            <label class=\"radio-inline\">
                <input type=\"radio\" name=\"index4\" value='1' ".$checkedSex2.">Женский
            </label>
            <br>
            ".$editadminlevel."
            <br>
            <div class=\"form-group col-xs-12\">
                <label class=\"control-label col-xs-3\">Статус профиля:</label>
                <div class=\"col-xs-3\">
                  <select class=\"form-control\" name=\"index8\">
                    <option value='0' ".$checkedStatus[0].">Не подтвержденный</option>
                    <option value='1' ".$checkedStatus[1].">Активный</option>
                    <option value='2' ".$checkedStatus[2].">В архиве</option>
                    <option value='3' ".$checkedStatus[3].">Удален</option>
                  </select>
                </div>
            </div>
            <br>
            <div class=\"input-group col-xs-4\">
              <span class=\"input-group-addon\">Pass</span>
              <input id=\"msg\" type=\"text\" class=\"form-control\" name=\"index6\">
            </div>
            <br>
            <div class=\"input-group col-xs-4 ".$norepass."\">
              <span class=\"input-group-addon\">Re pass</span>
              <input id=\"msg\" type=\"text\" class=\"form-control\" name=\"index7\">
            </div>
              ".$norepass2."
            <br>
            <br>
            <div class=\"form-group\">
                <input type=\"submit\" name=\"EditUser\" class=\"btn btn-primary\" value=\"Изменить\">
            </div>
        </form>
        ";
        return $textresult;
    }
}