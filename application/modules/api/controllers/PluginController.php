<?php

class PluginController extends PluginModel
{
    static function GetInfoText()
    {
        $obj = json_decode(PluginModel::GetPluginModel());
        $maxnum=$obj->{'maxdata'};
        $textresult = getLang("PLUGINS_1") . ":<br><br>";
        if($maxnum>0){
            for ($i = 1; $i <= $maxnum; $i++) {
                $use = $obj->{'result'.$i}->{'active'};
                $id = $obj->{'result'.$i}->{'id'};
                $lang1 = $obj->{'result'.$i}->{'lang-name'};
                $lang2 = $obj->{'result'.$i}->{'lang-info'};
                if ($use == 0) $status = getLang("PLUGINS_5");
                else $status = getLang("PLUGINS_4");
                $textresult .= "____________________________________________________________________________________<br>";
                $textresult .= getLang("PLUGINS_2") . ": " . getLang($lang1) . "<br>";
                $textresult .= getLang("PLUGINS_6") . ": " . getLang($lang2) . "<br>";
                $textresult .= getLang("PLUGINS_3") . ": " . $status . "<br>";
                if ($use == 0) {
                    $textresult .= "<a href=\"http://".$_SERVER["HTTP_HOST"]."/admin/list/2/id/".$id."/active/1/\">" . getLang("PLUGINS_7") . "</a><br>";
                } else {
                    $textresult .= "<a href=\"http://".$_SERVER["HTTP_HOST"]."/admin/list/2/id/".$id."/active/0/\">" . getLang("PLUGINS_8") . "</a><br>";
                }
            }
        }
        echo $textresult;
    }
}
