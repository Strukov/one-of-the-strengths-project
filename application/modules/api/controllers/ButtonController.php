<?php

class ButtonController extends ButtonModel
{
    static function GetButtonText()
    {
        $obj = json_decode(ButtonModel::GetButtonModel());
        $maxnum=$obj->{'maxdata'};
        $textresult="";
        if($maxnum>0) {
            for ($i = 1; $i <= $maxnum; $i++) {
                $id = $obj->{'result'.$i}->{'id'}+100;
                $use = $obj->{'result'.$i}->{'active'};
                $lang1 = $obj->{'result'.$i}->{'lang-name'};
                if($use==0)continue;
                $textresult.="<li><button type='button' class='btn btn-light' onclick=\"location.href='http://".$_SERVER["HTTP_HOST"]."/admin/list/".$id."'\">".getLang($lang1)."</button></li>";
            }
        }
        return $textresult;
    }
}