<?php

class AccountController extends AccountModel
{
    static function GetAccountDataTable($max_list=1,$filter="")
    {
        if(strlen($filter)){
            $obj = json_decode(AccountModel::GetAccountModel($max_list,$filter));
        }else{
            $obj = json_decode(AccountModel::GetAccountModel($max_list));
        }
        $textresult="
            <form name=\"login\" method=\"POST\" action=\"/ControllerMethod.php\">
            <b>фильтр:</b>
            <input type=\"text\" name=\"index1\" size=\"40\" value=\"".$_SESSION['FilterUser']."\">
            <input type=\"submit\" name=\"FilterUser\" value=\"Отправить\">
            <input type=\"submit\" name=\"ClearFilterUser\" value=\"Сбросить\">
            </form>
        ";
        $_SESSION['indexURL']=$_SERVER['REQUEST_URI'];
        $maxnum=$obj->{'maxdata'};
        $textresult.="
            <table class=\"table table-hover table-bordered\">
            <thead><tr class='bg-success'>
            <th>#<br></th>
            <th>Nick</th>
            <th>Email</th>
            <th>Admin Level<br></th>
            <th>Reg IP</th>
            <th>Use IP</th>
            <th>Register Date<br></th>
            <th>last login at</th>
            <th>Status</th>
            </tr></thead><tbody>
        ";
        if($maxnum>0) {
            for ($i = 1; $i <= $maxnum; $i++) {
                $textresult.="
                    <tr class=\"movelink\" data-href=\"http://".$_SERVER["HTTP_HOST"]."/admin/list/1/id/".$obj->{'result'.$i}->{'id'}."/\">
                    <th>".$obj->{'result'.$i}->{'id'}."</th>
                    <td>".$obj->{'result'.$i}->{'login'}."</td>
                    <td>".$obj->{'result'.$i}->{'email'}."</td>
                    <td>".GetAdminLevel($obj->{'result'.$i}->{'id'})."</td>
                    <td>".$obj->{'result'.$i}->{'reg_ip'}."</td>
                    <td>".$obj->{'result'.$i}->{'use_ip'}."</td>
                    <td>".$obj->{'result'.$i}->{'created_at'}."</td>
                    <td>".$obj->{'result'.$i}->{'last_login_at'}."</td>
                ";
                if($obj->{'result'.$i}->{'active'}==1)$textresult.="<td>Active</td>";
                else if($obj->{'result'.$i}->{'archive'}==1)$textresult.="<td>Archive</td>";
                else if($obj->{'result'.$i}->{'deleted'}==1)$textresult.="<td>Deleted</td>";
                else $textresult.="<td>Not active</td>";
                $textresult.="</tr>";
            }
        }
        $textresult.="</tbody></table>";
        if($max_list>1)$textresult.="<a href='http://".$_SERVER["HTTP_HOST"]."/admin/list/1/mli/".($max_list-1)."'>назад</a>";
        $textresult.=" [стр.".$max_list."] ";
        if($obj->{'maxuser'}>MAX_DISPLAY_LIST)$textresult.="<a href='http://".$_SERVER["HTTP_HOST"]."/admin/list/1/mli/".($max_list+1)."'>вперед</a>";
        return $textresult;
    }
}