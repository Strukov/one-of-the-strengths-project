<?php
    ob_start(); // Initiate the output buffer
?>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html" />
    <meta name="author" content="" />
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, user-scalable=0" />

	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	
	<link rel="stylesheet" href="/css/style.css">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.bundle.min.js"></script>

    <title><?php echo getLang("ADMIN_PANEL_1");?></title>
</head>

<body>
<header> 

</header>

<br class="container_content">
    <p>
        Войдите в аккаунт
    </p>
    <?php
        session_start();
        unset($_SESSION['indexURL']);
        $_SESSION['indexURL']=$_SERVER['REQUEST_URI'];
        if($_SESSION['errorNoAcc']===1){
            echo "<span style=\"background:#FFCCCC\">Аккаунт не найден!</span>";
        }
        if($_SESSION['errorNoAcc']===2){
            echo "<span style=\"background:#FFCCCC\">Неверный пароль от аккаунта!</span>";
        }
        if($_SESSION['errorNoAcc']===3){
            echo "<span style=\"background:#FFCCCC\">Аккаунт не имеет прав доступа администратора!</span>";
        }
        if($_SESSION['errorNoAcc']===4){
            echo "<span style=\"background:#FFCCCC\">Аккаунт не имеет достаточных прав администратора!</span>";
        }
    ?>
    <br>
    <br>
    <form name="login" method="POST" action="/ControllerMethod.php">
        <div class="input-group <?php if($_SESSION['errorNoAcc']!=2){echo "has-error";}?> col-xs-3">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input id="email" type="text" class="form-control" name="index1" placeholder="Email">
        </div>
        <br>
        <br/>
        <div class="input-group <?php if($_SESSION['errorNoAcc']===2){echo "has-error";}?> col-xs-3">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input id="password" type="password" class="form-control" name="index2" placeholder="Password">
        </div>
        <br>
        <button type="submit" name="UserLogin" class="btn btn-primary">Войти</button>
    </form>
    <?php unset($_SESSION['errorNoAcc']);?>
</div>
<footer>

</footer>

</body>
</html>
<?php
    ob_end_flush(); // Flush the output from the buffer
?>
