<?php

class Core
{
    static function ConnectBD($request="")
    {
        $DB_HOST = "127.0.0.1"; // Хост
        $DB_USER = "root"; // Логин
        $DB_PASS = "";// Пароль
        $DB_NAME = "adminpanel"; // Таблица Базы Данных
        $db = mysqli_connect($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
        if (!$db)
        {
            return "Ошибка: <br>Невозможно установить соединение с MySQL.<br>Код ошибки errno: ".mysqli_connect_errno()."<br>Текст ошибки error: ".mysqli_connect_error();
        }
        else
        {
            mysqli_set_charset($db, "utf8");// установка кодировки на utf8
            $result = mysqli_query($db, $request) or $error_code="Ошибка " . mysqli_error($db);
            mysqli_close($db);
            if ($result)
                return $result;//json_encode(mysqli_fetch_assoc($result));
            else{
                return $error_code;
            }
        }
    }
    static function GetUserAvatar($userID=0){
        $obj=json_decode(UserModel::GetUserModel($userID));
        $avatar=$obj->{'images'};
        if(strlen($avatar)<=0||!file_exists("./images/user/".$avatar)){
            if($obj->{'sex'}==1)
                $avatar="no_avatar_j.png";
            else $avatar="no_avatar_m.png";
        }
        return $avatar;
    }
    static function GetUserIDByEmail($login=""){
        if(strlen($login)<=0)return 0;
        $result=Core::ConnectBD("SELECT `id` FROM `user` WHERE `email`='".$login."'");
        $obj=json_decode(json_encode(mysqli_fetch_assoc($result)));
        if($obj->{'id'}<1)return 0;
        return $obj->{'id'};
    }
    static function GetUserNameByID($userID=""){
        if(strlen($userID)<=0)return "None";
        $result=Core::ConnectBD("SELECT `login` FROM `user` WHERE `id`='".$userID."'");
        $obj=json_decode(json_encode(mysqli_fetch_assoc($result)));
        if(strlen($obj->{'login'})<=0)return "None";
        return $obj->{'login'};
    }
    static function GetUserNameByEmail($userEmail=""){
        if(strlen($userEmail)<=0)return "None";
        $result=Core::ConnectBD("SELECT `login` FROM `user` WHERE `email`='".$userEmail."'");
        $obj=json_decode(json_encode(mysqli_fetch_assoc($result)));
        if(strlen($obj->{'login'})<=0)return "None";
        return $obj->{'login'};
    }
    static function GetUserParameter($parameter1="",$parameter2="",$parameter3="",$parameter4=""){
        if(strlen($parameter1)<=0)return "None";
        if(strlen($parameter2)<=0)return "None";
        $result=Core::ConnectBD("SELECT `".$parameter1."` FROM `".$parameter2."` WHERE `".$parameter3."`='".$parameter4."'");
        $obj=json_decode(json_encode(mysqli_fetch_assoc($result)));
        if(strlen($obj->{$parameter1})<=0)return "None";
        return $obj->{$parameter1};
    }
    static function GetAdminLevel($userID=0){
        $admin=0;
        if($userID==0)return $admin;
        $result = Core::ConnectBD("SELECT `admin_access` FROM `admin` WHERE `active`='1' AND `user`='".$userID."'");
        $obj=json_decode(json_encode(mysqli_fetch_assoc($result)));
        if(strlen($obj->{'admin_access'})) $admin=$obj->{'admin_access'};
        return $admin;
    }
    static function AdminLevelAccess($level,$userlevel){
        if($userlevel<$level)return false;
        return true;
    }
    static function CreatTokenKey(){
        $tokengen = array("1","2","3","4","5","6","7","8","9","0","q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m");
        $token="";
        for($i=0;$i<count($tokengen);$i++){
            $token.=$tokengen[rand(0, count($tokengen))];
        }
        return $token;
    }
    static public function getLang($num="0")
    {
        if(@$_COOKIE['langinfo']==1) // en
        {
            $Lang = array(
                "0"=>"No text translation! ID = ".$num,
                ////////////////////// Основное
                "ADMIN_PANEL_1"=>"Admin panel",
                ////////////////////// Меню
                "MENU_1"=>"Language",
                "MENU_2"=>"Setting",
                "MENU_3"=>"Exit",
                ////////////////////// Кнопки
                "BUTTON_1"=>"Accounts",
                "BUTTON_2"=>"Plugins",
                "BUTTON_3"=>"News feed",
                "BUTTON_400"=>"Add Accounts (temporarily)",
                ////////////////////// Плагины
                "PLUGINS_1"=>"List of available plugins",
                "PLUGINS_2"=>"Title",
                "PLUGINS_3"=>"Plugin status",
                "PLUGINS_4"=>"Works",
                "PLUGINS_5"=>"Does not work",
                "PLUGINS_6"=>"Description",
                "PLUGINS_7"=>"Turn on",
                "PLUGINS_8"=>"Turn off",
                ////////////////////// Информация плагинов
                "INFO_PLUGINS_1"=>"This plugin adds the ability to publish news in the news feed. To start using, make sure this plugin is enabled.",
                "INFO_PLUGINS_400"=>"Temporary button to create a new user.",
            );
        }
        else// ru
        {
            $Lang = array(
                "0"=>"Отсутствует перевод текста! ID = ".$num,
                ////////////////////// Основное
                "ADMIN_PANEL_1"=>"Админ панель",
                ////////////////////// Меню
                "MENU_1"=>"Язык",
                "MENU_2"=>"Настройки",
                "MENU_3"=>"Выйти",
                ////////////////////// Кнопки
                "BUTTON_1"=>"Аккаунты",
                "BUTTON_2"=>"Плагины",
                "BUTTON_3"=>"Новостная лента",
                "BUTTON_400"=>"Добавить аккаунт (временно)",
                ////////////////////// Плагины
                "PLUGINS_1"=>"Список доступных плагинов",
                "PLUGINS_2"=>"Название",
                "PLUGINS_3"=>"Состояние плагина",
                "PLUGINS_4"=>"Включен",
                "PLUGINS_5"=>"Выключен",
                "PLUGINS_6"=>"Описание",
                "PLUGINS_7"=>"Включить",
                "PLUGINS_8"=>"Выключить",
                ////////////////////// Информация плагинов
                "INFO_PLUGINS_1"=>"Данный плагин добавляет возможность публиковать новости в новостной ленте. Для начала использования, убедитесь что данный плагин включен.",
                "INFO_PLUGINS_400"=>"Временная кнопка для создание нового пользователя.",
            );
        }
        if(is_null($Lang[$num])){
            return $Lang['0'];
        }
        return $Lang[$num];
    }
}