<?php
    session_start();

    define ("MAX_DISPLAY_LIST", 20);

    require_once('application/core/Core.php');
    require_once('application/core/ActiveRecord.php');
    require_once('application/modules/api/models/PluginModel.php');
    require_once('application/modules/api/controllers/PluginController.php');
    require_once('application/modules/api/models/ButtonModel.php');
    require_once('application/modules/api/controllers/ButtonController.php');
    require_once('application/modules/api/models/AccountModel.php');
    require_once('application/modules/api/controllers/AccountController.php');
    require_once('application/modules/api/models/UserModel.php');
    require_once('application/modules/api/controllers/UserController.php');
    require_once('application/modules/api/models/JsonModel.php');

    function getLang($num){
        return Core::getLang($num);
    }
    function FilterUrl($str){
        $result = str_replace($str, '', $_SERVER['REQUEST_URI']);
        for($i=0;$i<5;$i++)$result = str_replace("//", "/", $result);
        header('Location:http://' .$_SERVER["HTTP_HOST"]. $result);
    }
    function GetInfoURL($str){
        $url = explode('/',strtolower(substr($_SERVER['REQUEST_URI'], 1)));
        $_SESSION['indexURL']=$_SERVER['REQUEST_URI'];
        for($i=0;$i<=substr_count(substr($_SERVER['REQUEST_URI'], 1),'/');$i++){
            if($url[$i]===$str&&$str==='language'){
                SetCookie("langinfo", $url[$i+1], time() + (60 * 60 * 24 * 30 * 12), '/');
                FilterUrl("language/".$url[$i+1]);
                return 0;
            }
            if($url[$i]===$str&&$str==='setting'){
                echo "Здесь могли бы быть настройки аккаунта, но их нет!";
                return 0;
            }
            if($url[$i]===$str&&$str==='exit'){
                SetCookie("info1", "", 0, '/');
                SetCookie("info2", "", 0, '/');
                SetCookie("info3", "", 0, '/');
                SetCookie("info4", "", 0, '/');
                FilterUrl("exit");
            }
            if($url[$i]===$str&&$str==='list'){
                switch($url[$i+1]){
                    case 1:{// button account
                        if($url[$i+2]=='mli'){
                            echo AccountController::GetAccountDataTable($url[$i+3],$_SESSION['FilterUser']);
                            return 0;
                        }
                        else if($url[$i+2]=='id'){
                            if($url[$i+4]=='edit'){
                                unset($_SESSION['indexURL']);
                                $_SESSION['indexURL']=$_SERVER['REQUEST_URI'];
                                echo UserController::EditUserData($url[$i+3]);
                            }
                            else echo UserController::GetUserData($url[$i+3]);
                            return 0;
                        }
                        else{
                            echo AccountController::GetAccountDataTable(1,$_SESSION['FilterUser']);
                        }
                        return 0;
                    }
                    case 2:{// button plugins
                        if($url[$i+2]=='id'&&$url[$i+4]=='active'){// 3 - параметр id | 5 - параметр active
                            Core::ConnectBD("UPDATE `use_plugins` SET `active`='".$url[$i+5]."' WHERE `ID`='".$url[$i+3]."'");
                            FilterUrl($url[$i+2]."/".$url[$i+3]."/".$url[$i+4]."/".$url[$i+5]);
                            break(3);
                        }
                        else echo PluginController::GetInfoText();
                        return 0;
                    }
                    case 101:{
                        echo $_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI'];
                        return 0;
                    }
                    case 102:{
                        if($url[$i+2]=='regstage'){
                            if($url[$i+3]=='successfulstage'){
                                echo UserController::SuccessСonfirmationUserData();
                            }
                            else echo UserController::СonfirmationUserData();
                        }
                        else{
                            echo UserController::AddUserData();
                        }
                        return 0;
                    }
                }
            }
        }
        return 1;
    }
    function GetAdminLevel($userID=0){
        return Core::GetAdminLevel($userID);
    }
    function GetUserAvatar($userID=0){
        return Core::GetUserAvatar($userID);
    }
    function GetUserIDByEmail($login=""){
        return Core::GetUserIDByEmail($login);
    }
    function GetUserNameByID($userID=""){
        return Core::GetUserNameByID($userID);
    }
    function GetUserNameByEmail($userEmail=""){
        return Core::GetUserNameByEmail($userEmail);
    }
    function GetUserParameter($parameter1="",$parameter2="",$parameter3="",$parameter4=""){ // Выгрузит параметр $parameter1 из таблицы $parameter2 Если $parameter3 равен $parameter4
        return Core::GetUserParameter($parameter1,$parameter2,$parameter3,$parameter4);
    }
    function AdminLevelAccess($level,$userlevel){
        return Core::AdminLevelAccess($level,$userlevel);
    }
    $url = explode('/',strtolower(substr($_SERVER['REQUEST_URI'], 1)));
    $dirindex="";
    if($url[0]=='admin'||$url[0]=='user') {
        $dirindex = $url[0];
    }
    switch ($url[0]) {
        case '':{
            include('global.php');
            break;
        }
        case 'admin':{
            if(!@$_COOKIE['info3']){
                include('application/Account/authorization.php');
            }
            else{
                include "admin.php";
            }
            break;
        }
    }
?>